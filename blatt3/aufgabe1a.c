#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <errno.h>

#define READ_NUM 5
#define WRITE_NUM 2
#define NUM_SIMLOOP 3
#define WAIT_TIME 1


void init_sem(int32_t sem_id) {

  if (semctl(sem_id, 0, SETVAL, 1) > 0) {

    perror("Error in semctl");
    exit(1);
  }
}

void P(int32_t sem_num, int32_t sem_id) {

  struct sembuf semaphore;
  semaphore.sem_num = sem_num;
  semaphore.sem_op = 1;
  semaphore.sem_flg = ~(IPC_NOWAIT | SEM_UNDO);
  
  if(semop(sem_id, &semaphore, 1)) {

    perror("Error in P");
    exit(1);
  }
}

void V(int32_t sem_num, int32_t sem_id) {

  struct sembuf semaphore;
  semaphore.sem_num = sem_num;
  semaphore.sem_op = -1;
  semaphore.sem_flg = ~(IPC_NOWAIT | SEM_UNDO);
  
  if(semop(sem_id, &semaphore, 1)) {
    
    perror("Error in V");
    exit(1);
  }
}

void _write(int32_t sem_id){
  semctl(sem_id, 0, SETVAL,semctl(sem_id, 0, GETVAL)+1);
}

int _read(int32_t sem_id){
  return semctl(sem_id, 0, GETVAL);
}

int32_t main() {

  key_t sem_key = ftok("/tmp/sem", 1);
  key_t var_key = ftok("/tmp/sem", 2);
  int32_t sem_id, var_id;

  if (sem_key < 0 || var_key < 0) {
    
    perror("Error in ftok");
    exit(1);
  }

  sem_id = semget(sem_key, 1, IPC_CREAT | 0666);
  var_id = semget(var_key, 1, IPC_CREAT | 0666);
  
  if (sem_id < 0 || var_id < 0) {
    
    perror("Error in semget");
    exit(1);
  }

  init_sem(sem_id);
  init_sem(var_id);
  V(0, var_id);

  for (uint8_t i = 0; i < WRITE_NUM + READ_NUM; i++) {
    pid_t pid = fork();

    if (pid == 0) {
      printf("[Prozess-Nr: %d / PID: %d]\n", i, getpid());
      if(i < WRITE_NUM){ //writer
        for(int i = 0; i< NUM_SIMLOOP; i++){
          printf("Write\n");
          sleep(1);
          printf("Write done\n");
          sleep(1);
        }
      }else{ //reader
        for(int i = 0; i< NUM_SIMLOOP; i++){
          printf("Read\n");
          sleep(1);
          printf("Read done\n");
          sleep(1);
        }
      }
    exit(0);
    }
    else if (pid == -1) {

      perror("fork() fehlerhaft.\n");
      exit(1);
    }
  }
}
