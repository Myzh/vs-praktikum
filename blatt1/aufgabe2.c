#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <errno.h>

#define NUM_CHILDS 3
#define NUM_SIMLOOP 3
#define WAIT_TIME 1

int32_t sem_id;

void init_sem() {

  if (semctl(sem_id, 0, SETVAL, 1) > 0) {

    perror("Error in semctl");
    exit(1);
  }
}

void P(int32_t sem_num) {

  struct sembuf semaphore;
  semaphore.sem_num = sem_num;
  semaphore.sem_op = 1;
  semaphore.sem_flg = ~(IPC_NOWAIT | SEM_UNDO);
  
  if(semop(sem_id, &semaphore, 1)) {

    perror("Error in P");
    exit(1);
  }
}

void V(int32_t sem_num) {

  struct sembuf semaphore;
  semaphore.sem_num = sem_num;
  semaphore.sem_op = -1;
  semaphore.sem_flg = ~(IPC_NOWAIT | SEM_UNDO);
  
  if(semop(sem_id, &semaphore, 1)) {
    
    perror("Error in V");
    exit(1);
  }
}

int32_t main() {

  key_t sem_key = ftok("/tmp/sem", 1);

  if (sem_key < 0) {
    
    perror("Error in ftok");
    exit(1);
  }

  sem_id = semget(sem_key, 1, IPC_CREAT | 0666);
  
  if (sem_id < 0) {
    
    perror("Error in semget");
    exit(1);
  }

  init_sem();

  for (uint8_t i = 0; i < NUM_CHILDS; i++) {

    pid_t pid = fork();

    if (pid == 0) {

      printf("[Prozess-Nr: %d / PID: %d]\n", i, getpid());

      for (uint8_t j = 0; j < NUM_SIMLOOP; j++) {

        V(0);
        
        printf("Prozess %i betritt kritischen Bereich\n", i);
        sleep(WAIT_TIME);
        printf("Prozess %i verlässt kritischen Bereich\n", i);

        P(0);

        printf("Prozess %i betritt nicht-kritischen Bereich\n", i);
        sleep(WAIT_TIME);
        printf("Prozess %i verlässt nicht-kritischen Bereich\n", i);
      }

      exit(0);
    }
    else if (pid == -1) {

      perror("fork() fehlerhaft.\n");
      exit(1);
    }
  }
  
  getchar();
}
