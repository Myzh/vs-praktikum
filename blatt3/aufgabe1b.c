#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <errno.h>

#define READ_NUM 5
#define WRITE_NUM 2
#define NUM_SIMLOOP 3
#define WAIT_TIME 1

int32_t reader = 0;

void init_sem(int32_t sem_id) {

  if (semctl(sem_id, 0, SETVAL, 1) > 0) {

    perror("Error in semctl");
    exit(1);
  }
}

void P(int32_t sem_num, int32_t sem_id) {

  struct sembuf semaphore;
  semaphore.sem_num = sem_num;
  semaphore.sem_op = -1;
  semaphore.sem_flg = ~(IPC_NOWAIT | SEM_UNDO);
  
  if(semop(sem_id, &semaphore, 1)) {

    perror("Error in P");
    exit(1);
  }
}

void V(int32_t sem_num, int32_t sem_id) {

  struct sembuf semaphore;
  semaphore.sem_num = sem_num;
  semaphore.sem_op = 1;
  semaphore.sem_flg = ~(IPC_NOWAIT | SEM_UNDO);
  
  if(semop(sem_id, &semaphore, 1)) {
    
    perror("Error in V");
    exit(1);
  }
}

void _write(int32_t sem_id){
  int tmp = semctl(sem_id, 0, GETVAL)+1;
  printf("Write: %d\n", tmp);
  semctl(sem_id, 0, SETVAL,tmp);
}

int _read(int32_t sem_id){
  int tmp = semctl(sem_id, 0, GETVAL);
  printf("Read: %d\n", tmp);
  return tmp;
}

int32_t main() {

  key_t writer_key = ftok("/tmp/sem", 1);
  key_t mutex_key = ftok("/tmp/sem", 2);
  key_t var_key = ftok("/tmp/sem", 3);
  int32_t writer_id, mutex_id, reader_id;

  if (writer_key < 0 || var_key < 0) {
    
    perror("Error in ftok");
    exit(1);
  }

  writer_id = semget(writer_key, 1, IPC_CREAT | 0666);
  mutex_id = semget(mutex_key, 1, IPC_CREAT | 0666);
  reader_id = semget(var_key, 1, IPC_CREAT | 0666);
  
  if (writer_id < 0 || reader_id < 0) {
    
    perror("Error in semget");
    exit(1);
  }

  init_sem(writer_id);
  init_sem(mutex_id);
  init_sem(reader_id);
  semctl(reader_id, 0, SETVAL,0);

  for (uint8_t i = 0; i < WRITE_NUM + READ_NUM; i++) {
    pid_t pid = fork();

    if (pid == 0) {
      printf("[Prozess-Nr: %d / PID: %d]\n", i, getpid());
      if(i < WRITE_NUM){ //writer
        for(int k = 0; k< NUM_SIMLOOP; k++){
          P(0, writer_id);
          //_write(reader_id);
          //printf("%d\n", semctl(writer_id, 0, GETVAL));
          //printf("Write: %d\n", semctl(reader_id, 0, GETVAL));
          printf("Write\n");
          sleep(1);
          printf("Write done\n");
          V(0,writer_id);
          //printf("Write: %d\n", semctl(reader_id, 0, GETVAL));
          sleep(1);
        }
      }else{ //reader
        for(int i = 0; i< NUM_SIMLOOP; i++){
          P(0,mutex_id);
          semctl(reader_id, 0, SETVAL,semctl(reader_id, 0, GETVAL)+1);
          if(semctl(reader_id, 0, GETVAL) == 1){
            P(0,writer_id);
          }
          V(0,mutex_id);

          printf("Read\n");
          //printf("Read: %d\n", tmp);
          sleep(1);
          printf("Read done\n");

          P(0,mutex_id);
          semctl(reader_id, 0, SETVAL,semctl(reader_id, 0, GETVAL)-1);
          if(semctl(reader_id, 0, GETVAL) == 0){
            printf("Writer freigeben\n");
            V(0,writer_id);
          }
          V(0,mutex_id);
          //printf("Read: %d\n", tmp);
          sleep(1);
        }
      }
    exit(0);
    }
    else if (pid == -1) {

      perror("fork() fehlerhaft.\n");
      exit(1);
    }
  }
  getchar();
}
