#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <errno.h>


#define NUM_CHILDS 5
#define NUM_SIMLOOP 3
#define WAIT_TIME_MIN 1000000 // in microseconds
#define WAIT_TIME_MAX 3000000


int32_t sem_id;

void init_sem() {

  for (uint8_t i = 0; i < NUM_CHILDS; i++) {
    if (semctl(sem_id, i, SETVAL, 1) > 0) {

      perror("Error in semctl");
      exit(1);
    }
  }
}

void P(int32_t sem_num_1, int32_t sem_num_2) {

  struct sembuf semaphores[2];
  semaphores[0].sem_num = sem_num_1;
  semaphores[0].sem_op = 1;
  semaphores[0].sem_flg = ~(IPC_NOWAIT | SEM_UNDO);
  semaphores[1].sem_num = sem_num_2;
  semaphores[1].sem_op = 1;
  semaphores[1].sem_flg = ~(IPC_NOWAIT | SEM_UNDO);
  
  if(semop(sem_id, semaphores, 2)) {

    perror("Error in P");
    exit(1);
  }
}

void V(int32_t sem_num_1, int32_t sem_num_2) {

  struct sembuf semaphores[2];
  semaphores[0].sem_num = sem_num_1;
  semaphores[0].sem_op = -1;
  semaphores[0].sem_flg = ~(IPC_NOWAIT | SEM_UNDO);
  semaphores[1].sem_num = sem_num_2;
  semaphores[1].sem_op = -1;
  semaphores[1].sem_flg = ~(IPC_NOWAIT | SEM_UNDO);
  
  if(semop(sem_id, semaphores, 2)) {
    
    perror("Error in V");
    exit(1);
  }
}

int32_t main() {

  key_t sem_key = ftok("/tmp/sem", 1);

  if (sem_key < 0) {
    
    perror("Error in ftok");
    exit(1);
  }

  sem_id = semget(sem_key, 5, IPC_CREAT | 0666);
  
  if (sem_id < 0) {
    
    perror("Error in semget");
    exit(1);
  }

  init_sem();

  for (uint8_t i = 0; i < NUM_CHILDS; i++) {

    pid_t pid = fork();

    if (pid == 0) {

      printf("[Philosoph-Nr: %d / PID: %d]\n", i, getpid());

      for (uint8_t j = 0; j < NUM_SIMLOOP; j++) {

        V(i, (i + 1) % 5);
        
        printf("Philosoph %i fängt an zu essen\n", i);
        usleep(WAIT_TIME_MIN + rand() % (WAIT_TIME_MAX + 1 - WAIT_TIME_MIN));
        
        printf("Philosoph %i ist fertig mit essen\n", i);

        P(i, (i + 1) % 5);
        usleep(WAIT_TIME_MIN + rand() % (WAIT_TIME_MAX + 1 - WAIT_TIME_MIN));
        printf("Philosoph %i ist hungrig\n", i);
      }

      printf("Philosoph %i verlässt den Tisch\n", i);
      exit(0);
    }
    else if (pid == -1) {

      perror("fork() fehlerhaft.\n");
      exit(1);
    }
  }
  getchar();
}
