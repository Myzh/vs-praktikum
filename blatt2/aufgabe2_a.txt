# initialisieren von semaphoren mit 1
semaphoren[5]

# starten von 5 threads mit jeweils dem index i:
    while true:
        sem_1 = i
        sem_2 = (i + 1) % 5

        if (i % 2) == 0:
            # vertausche sem_1 und sem_2
            sem_1, sem_2 = sem_2, sem_1

        V(sem_1)
        V(sem_2)

        wait(random())

        P(sem_1)
        P(sem_2)

        wait(random())