#include "math.h"
#include <rpc/rpc.h>

int * add_1_svc(intpair *pair, struct svc_req *rqstp) {

  int *ret = (int *)malloc(sizeof(int));

  *ret = pair->a + pair->b;

  return ret;
}

int * multiply_1_svc(intpair *pair, struct svc_req *rqstp) {

  int *ret = (int *)malloc(sizeof(int));

  *ret = pair->a * pair->b;

  return ret;
}

int * cube_1_svc(int *input, struct svc_req *rqstp) {
  
  int *ret = (int *)malloc(sizeof(int));

  *ret = *input * *input * *input;

  return ret;
}
