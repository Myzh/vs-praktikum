#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

#define NUM_CHILDS 3
#define NUM_SIMLOOP 3
#define WAIT_TIME 1

int32_t main() {
  
  for (uint8_t i = 0; i < NUM_CHILDS; i++) {

    pid_t pid = fork();

    if (pid == 0) {

      printf("[Prozess-Nr: %d / PID: %d]\n", i, getpid());

      for (uint8_t j = 0; j < NUM_SIMLOOP; j++) {

        printf("Prozess %i betritt kritischen Bereich\n", i);
        sleep(WAIT_TIME);
        printf("Prozess %i verlässt kritischen Bereich\n", i);

        printf("Prozess %i betritt nicht-kritischen Bereich\n", i);
        sleep(WAIT_TIME);
        printf("Prozess %i verlässt nicht-kritischen Bereich\n", i);
      }

      exit(0);
    }
    else if (pid == -1) {

      perror("fork() fehlerhaft.\n");
      exit(1);
    }
  }
  
  getchar();
}
