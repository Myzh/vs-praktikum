#include "math.h"
#include <rpc/rpc.h>

int main(int argc, char**argv) {

  if (argc < 4) {

    printf("Invalid arguments.\n");
    return 1;
  }

  CLIENT *clnt = clnt_create(argv[1], MATHPROG, MATHVERS, "tcp");

  if (clnt == NULL) {

    printf("Cannot connect to server.\n");
    return 1;
  }

  intpair pair;
  pair.a = atoi(argv[2]);
  pair.b = atoi(argv[3]);

  int *res = add_1(&pair, clnt);

  printf("Result: %d\n", *res);

  return 0;
}
